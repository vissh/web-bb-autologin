$(document).ready(function () {

    var decodeParams = function (str) {
        return JSON.parse('{"' + decodeURIComponent(str.replace(/&/g, "\",\"").replace(/=/g,"\":\"")) + '"}');
    };

    var storage = chrome.storage.local;

    var serversId = '#servers';

    $(serversId).jtable({
        title: 'Сервера',
        sorting: true,
        
        actions: {
            listAction: function () {
                return $.Deferred(function (d) {
                    storage.get('servers', function (items) {
                        var records = [];
                        
                        $.each(items.servers || {}, function (url, server) {
                            server.url = url;
                            records.push(server);
                        });
                        
                        d.resolve({
                            Result: "OK",
                            Records: records,
                            TotalRecordCount: records.length
                        });
                    });
                });
            },
            
            deleteAction: function (record) {
                return $.Deferred(function (d) {
                    storage.get('servers', function (items) {
                        delete items.servers[record.url];
                        storage.set(items, function () {
                            d.resolve({"Result": "OK"});
                        });
                    });
                });
            },
            
            updateAction: function (stringParams) {
                return $.Deferred(function (d) {
                    storage.get('servers', function (items) {
                        var record = decodeParams(stringParams);
                        var server = items.servers[record.url];
                        server.name = record.name;
                        storage.set(items, function () {
                            d.resolve({
                                Result: 'OK',
                                Record: record
                            });
                        });
                    });
                });
            },
            
            createAction: function (stringParams) {
                return $.Deferred(function (d) {
                    storage.get('servers', function (items) {
                        var record = decodeParams(stringParams);

                        if (record.url[record.length - 1] !== '/') {
                            record.url += '/';
                        }
                        
                        var server = {
                            name: record.name,
                            enterprises: {},
                            users: {}
                        };
                        
                        var servers = items.servers || {};
                        servers[record.url] = server;
                        storage.set({servers: servers}, function () {
                            window.location.reload();
                            // d.resolve({
                            //     Result: 'OK',
                            //     Record: record
                            // });
                        });
                    });
                });
            }
        },
        
        fields: {
            url: {
                title: 'Адрес',
                key: true,
                create: true
            },

            name: {
                title: 'Наименование',
            },

            enterprises: {
                title: 'Учреждения',
                width: '2%',
                sorting: false,
                edit: false,
                create: false,
                
                display: function (serverData) {
                    var el = $('<center><a href="#">Отобразить</a></center>');
                    el.click(openTable);
                    return el;
                    
                    function openTable() {
                        $(serversId).jtable('openChildTable', el.closest('tr'), {
                            title: 'Учреждения',
                            create: true,
                            edit: true,
                            
                            actions: {
                                listAction: listAction,
                                createAction: createAction,
                                updateAction: createAction,
                                deleteAction: deleteAction,
                            },
                            
                            fields: {
                                id: {
                                    title: 'id',
                                    key: true,
                                    create: true
                                },

                                name: {
                                    title: 'Наименование'
                                }
                            }
                        }, function (data) {
                            data.childTable.jtable('load');
                        });
                    }

                    function listAction() {
                        return $.Deferred(function (d) {
                            var records = [];
                            
                            $.each(serverData.record.enterprises, function (id, record) {
                                record.id = id;
                                records.push(record);
                            });
                            
                            d.resolve({
                                Result: 'OK',
                                Records: records,
                                TotalRecordCount: records.length
                            });
                        });

                    }

                    function createAction(stringParams) {
                        return $.Deferred(function (d) {
                            storage.get('servers', function (items) {
                                var record = decodeParams(stringParams);
                                
                                items.servers[serverData.record.url].enterprises[record.id] = {
                                    name: record.name
                                };

                                storage.set(items, function () {
                                    d.resolve({
                                        Result: 'OK',
                                        Record: record
                                    });
                                });
                            });
                        });
                    }

                    function deleteAction(record) {
                        return $.Deferred(function (d) {
                            storage.get('servers', function (items) {
                                delete items.servers[serverData.record.url].enterprises[record.id] ;
                                
                                storage.set(items, function () {
                                    d.resolve({"Result": "OK"});
                                });
                            });
                        });
                    }
                }
            },

            users: {
                title: 'Пользователи',
                width: '2%',
                sorting: false,
                create: false,
                edit: false,

                display: function (serverData) {
                    var el = $('<center><a href="#">Отобразить</a></center>');
                    el.click(openTable);
                    return el;

                    function openTable() {
                        $(serversId).jtable('openChildTable', el.closest('tr'), {
                            title: 'Пользователи',
                            create: true,
                            edit: true,
                            
                            actions: {
                                listAction: listAction,
                                createAction: createAction,
                                updateAction: createAction,
                                deleteAction: deleteAction,
                            },
                            
                            fields: {
                                login: {
                                    title: 'Логин',
                                    key: true,
                                    create: true
                                },

                                password: {
                                    title: 'Пароль'
                                }
                            }
                        }, function (data) {
                            data.childTable.jtable('load');
                        });
                    }

                    function listAction() {
                        return $.Deferred(function (d) {
                            var records = [];
                            
                            $.each(serverData.record.users, function (login, record) {
                                record.login = login;
                                records.push(record);
                            });
                            
                            d.resolve({
                                Result: 'OK',
                                Records: records,
                                TotalRecordCount: records.length
                            });
                        });

                    }

                    function createAction(stringParams) {
                        return $.Deferred(function (d) {
                            storage.get('servers', function (items) {
                                var record = decodeParams(stringParams);
                                
                                items.servers[serverData.record.url].users[record.login] = {
                                    password: record.password
                                };

                                storage.set(items, function () {
                                    d.resolve({
                                        Result: 'OK',
                                        Record: record
                                    });
                                });
                            });
                        });
                    }

                    function deleteAction(record) {
                        return $.Deferred(function (d) {
                            storage.get('servers', function (items) {
                                delete items.servers[serverData.record.url].users[record.login] ;
                                
                                storage.set(items, function () {
                                    d.resolve({"Result": "OK"});
                                });
                            });
                        });
                    }
                }
            }
        }
    });
    
    $(serversId).jtable('load');

    $('#file').bind('change', function () {
        var reader = new FileReader();
        
        reader.onload = function () {
            var servers = JSON.parse(this.result);
            chrome.storage.local.set({servers: servers}, function () {
                window.location.reload();
            });
        };

        reader.readAsText(this.files[0]);

    });

    $('#export').bind('click', function () {
        storage.get('servers', function (items) {
            window.open('data:application/json;charset=utf-8,' + JSON.stringify(items.servers || {}), '_blank');
        });
    });
});