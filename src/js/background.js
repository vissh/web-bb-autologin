'use strict';


chrome.runtime.onMessage.addListener(function (request, sender, callback) {
    if (request.method == 'submit') {
            var user = request.params.user,
            enterprise = request.params.enterprise,
            tabId = request.params.tabId;
        
        // Залогиниться и сменить учреждение
        if (user && enterprise) {
            entry(tabId, user, enterprise, function () {
                changeEnterprise(tabId, enterprise);
            });
        }

        // Залогиниться без смены учреждения
        else if (user && !enterprise) {
            entry(tabId, user);
        }

        // Просто сменить учреждение
        else if (!user && enterprise) {
            changeEnterprise(tabId, enterprise);
        }

        else {
            callback = null;
        }

        callback && callback();
    }
});


/**
 * Вход с выбором учреждения
 */
function entry(tabId, user, enterprise, afterLogin) {
    var onUpdated = function (updatedTabId, info) {
        if (updatedTabId == tabId && info.status == 'complete') {
            chrome.tabs.onUpdated.removeListener(onUpdated);
            afterLogin && afterLogin();
        }
    };
    
    chrome.tabs.onUpdated.addListener(onUpdated);
    chrome.tabs.executeScript(tabId, {code: getLoginCode(user, enterprise || {})});
}


/**
 * Смена учреждения
 */
function changeEnterprise(tabId, ent) {
    var details = {
        code: getChangeEnterpriseCode(ent)
    };

    chrome.tabs.executeScript(tabId, details);
}


/**
 * Возвращает код для входа
 * который будет внедрен и выполнен на странице
 */
function getLoginCode(user, enterprise) {
    return '' +
    'var login = \'' + user.login + '\', password = \'' + user.password + '\';' +
    'var changeEnterpriseScriptContent = "' + getChangeEnterpriseCode(enterprise) + '";' +
    'var framesLayout = document.getElementById(\'frames-layout\');' +
    'var iframeClassName = \'ui-iframe\';' +
    'var fn = function () {' +
    '   var iframe = document.getElementsByClassName(iframeClassName)[0];' +
    '   var _document = iframe && iframe.contentDocument || document,' +
    '       loginElement = _document.getElementById(\'login\'),' +
    '       passwordElement = _document.getElementById(\'password\');' +
    '   if (loginElement && passwordElement) {' +
    '      loginElement.value = login;' +
    '      passwordElement.value = password;' +
    '      (_document.getElementById(\'commit-button\') || _document.getElementById(\'commit\')).click();' +
    '      if (iframe && ' + enterprise.id + ') {' +
    '           iframe.addEventListener(\'load\', function () {' +
    '               iframe.removeEventListener(\'load\', arguments.callee);' +
    '               var scriptElement = this.contentDocument.createElement(\'script\');' +
    '               scriptElement.textContent = changeEnterpriseScriptContent;' +
    '               this.contentDocument.body.appendChild(scriptElement);' +
    '               scriptElement.parentNode.removeChild(scriptElement);' +
    '          });' +
    '      }' +
    '   }' +
    '};' +
    '' +
    'if (framesLayout) {' +
    '   var intervalId = setInterval(function () {' +
    '       var iframe = document.getElementsByClassName(iframeClassName)[0];' +
    '       var iframeDocument = iframe && iframe.contentDocument || null;' +
    '       if (iframeDocument && iframeDocument.readyState == \'complete\' && iframeDocument.getElementById(\'login\')) {' +
    '           clearInterval(intervalId);' +
    '           fn();' +
    '       }' +
    '   }, 200);' +
    '} else {' +
    '   fn();' +
    '}' +
    '';
}


/**
 * Возвращает код смены учреждения
 * который будет выполнен на странице
 */
function getChangeEnterpriseCode(ent) {
    return '' +
    'var iframe = document.getElementsByClassName(\'ui-iframe\')[0],' +
    '   _window = iframe && iframe.contentWindow || window,' +
    '   _document = _window.document;' +
    'var scriptElement = _document.createElement(\'script\');' +
    'scriptElement.textContent = \'window.changeGlobalEnterprise && changeGlobalEnterprise(' + ent.id + ');\';' +
    '_document.head.appendChild(scriptElement);' +
    'scriptElement.parentNode.removeChild(scriptElement);' +
    '';
}