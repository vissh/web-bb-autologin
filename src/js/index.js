'use strict';


/**
 * Получение активной вкладки
 */
function getActiveTab() {
    return new Promise(function (resolve) {
        chrome.tabs.query({active: true, currentWindow: true}, function (arrayOfTabs) {
            var activeTab = arrayOfTabs[0];
            resolve(activeTab)
        });
    });
}


/**
 * Открыть необходимый адрес
 */
function openServerAddress(server, project) {
    return function (tab) {
        return new Promise(function (resolve) {
            var url = server.url;
            if (project) {
                url += project;
            }
            // сравнить адреса, при несовпадении перейти по нужному адресу
            // игнорить localhost
            if ((tab.url.indexOf(url) !== -1) || (tab.url.indexOf('//127.') !== -1 || tab.url.indexOf('//localhost') !== -1)) {
                resolve(tab);
            } else {
                var updatedHandler = function (tabId, info) {
                    if (tab.id == tabId && info.status == 'complete') {
                        chrome.tabs.onUpdated.removeListener(updatedHandler);
                        resolve(tab);
                    }
                };

                chrome.tabs.onUpdated.addListener(updatedHandler);
                chrome.tabs.update(tab.id, {url: url});
            }
        });
    }
}


angular.module('autologin', [])


.controller('selectController', ['$scope', function ($scope) {
    chrome.storage.local.get('servers', function (items) {
        getActiveTab().then(function (tab) {
            var servers = [], server, users, enterprises;

            for (var url in items.servers || {}) {
                users = [];
                enterprises = [];

                server = {
                    url: url,
                    name: items.servers[url].name
                };

                for (var login in items.servers[url].users) {
                    users.push({
                        login: login,
                        password: items.servers[url].users[login].password
                    });
                }
                
                for (var id in items.servers[url].enterprises) {
                    enterprises.push({
                        id: id,
                        name: items.servers[url].enterprises[id].name
                    });
                }

                server.users = users;
                server.enterprises = enterprises;
                servers.push(server);

                if (tab.url.indexOf(url) !== -1) {
                    $scope.selectServer = server;
                }
            }

            $scope.servers = servers;
            $scope.$apply();
        });
    });

    $scope.submit = function () {
        getActiveTab().then(openServerAddress(this.selectServer, this.selectProject)).then(function (tab) {
            var user = this.selectUser;
            var enterprise = this.selectEnterprise;
            
            var params = {
                tabId: tab.id,
                user: user,
                enterprise: enterprise
            };
            
            chrome.runtime.sendMessage({method: 'submit', params: params}, function (response) {
                window.close();
            });
        
        }.bind(this));
    };
}]);